package roomzik

import grails.test.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class MelomanoServiceSpec extends Specification {

    MelomanoService melomanoService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Melomano(...).save(flush: true, failOnError: true)
        //new Melomano(...).save(flush: true, failOnError: true)
        //Melomano melomano = new Melomano(...).save(flush: true, failOnError: true)
        //new Melomano(...).save(flush: true, failOnError: true)
        //new Melomano(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //melomano.id
    }

    void "test get"() {
        setupData()

        expect:
        melomanoService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Melomano> melomanoList = melomanoService.list(max: 2, offset: 2)

        then:
        melomanoList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        melomanoService.count() == 5
    }

    void "test delete"() {
        Long melomanoId = setupData()

        expect:
        melomanoService.count() == 5

        when:
        melomanoService.delete(melomanoId)
        sessionFactory.currentSession.flush()

        then:
        melomanoService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Melomano melomano = new Melomano()
        melomanoService.save(melomano)

        then:
        melomano.id != null
    }
}
