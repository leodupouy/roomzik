package roomzik

class Melomano extends Persona {
    Set<Melomano> amigos = []
    String email
    Set<Musica> gustos = []
    Set<Playlist> playlists = []
    Set<Publicacion> noticias = []
    Set<Artista> artistas_siguidos = []
    static hasMany = {
        amigos : Melomano
        gustos : Musica
        artistas_siguidos : Artista
        noticias : Publicacion
        playlists : Playlist
    }
    static constraints = {
    }

    String toString(){
        "nombre : "+nombre+"\nemail : "+email
    }
}
