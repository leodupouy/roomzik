package roomzik

class Artista extends Persona{
    int premium
    Set<Musica> creaciones = []
    Set<Playlist> albums = []
    static hasMany = {
        creaciones : Musica
        albums : Playlist
    }
    static constraints = {
    }
    Musica addCreacion(Musica creacion){
    	creaciones.add(creacion)
    }
    Playlist addAlbum(Playlist album){
    	albums.add(album)
    }

    String toString(){
        "nombre "+nombre+"\npremium "+premium+"\nid\n\n"+id
    }
}

