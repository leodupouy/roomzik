package roomzik

class Musica {
    String nombre
    Set <Estilo_de_musica> estilos = []
    Set <Artista> artistas = []
    long escuchas
    String path 
    String musicaContentType 

    static constraints = {
        musicaContentType nullable: true
    }

    static mapping = {
        musicaBytes column: 'musica_bytes', sqlType: 'longblob' 
    }
    static belongsTo = [artistas: Artista]
    static hasMany = {
        estilos : Estilo_de_musica
        artistas : Artista
    }
}
