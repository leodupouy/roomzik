package roomzik


enum Tipo_publicacion {
		publicacion,
		artista,
		playlist,
		musica,
		normal
	}

class Publicacion {
	Persona editor
	String mensaje
	Tipo_publicacion tipo
    static constraints = {
    }
}
