package roomzik

class MusicaController {

	def musicaService
	def artistaService
    def index() { redirect(uri:"/")}
    def addMusica(MusicaCommand cmd){
    	if (!params.artistaId && !cmd){
    	//add a flash message to print on main page
    	redirect(uri: "/")
    	}
    	else{
        if (cmd == null) {
            notFound()
            return
        }

        if (cmd.hasErrors()) {
            respond(cmd.errors, model: [musica: cmd], view: 'error')
            return
        }
		String applicationPath = request.getSession().getServletContext().getRealPath("")
        def musica = musicaService.addMusica(cmd,applicationPath)

        if (musica == null) {
            notFound()
            return
        }

        if (musica.hasErrors()) {
            respond(musica.errors, model: [musica: musica], view: 'error')
            return
        }

        //request.withFormat {
         //   form multipartForm {
        //        flash.message = message(code: 'default.updated.message', args: [message(code: 'musica.label', default: 'musica'), musica.id])
        //        redirect musica
        //    }
        //    '*' { respond musica, [status: OK] }
        //}

        redirect action: 'consultar', params:[musicaId: musica.id]
    	}
    		//else{
    		//[artistaId:params.artistaId]
    		//}
    }
    
    def leer(Musica musica) {
        if (musica == null){
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        render file: musica.musicaBytes, contentType: musica.musicaContentType
    }
    def notFound(){
    	render "notFound"
    }
    
    def delete(){
    	if(params.musicaId){
    		Musica musica = Musica.get(params.musicaId)
    		musica.delete()
    			
    	}
    }
    def consultar(){
    	[musica :Musica.get(params.musicaId)]
    	//todo
	}
}

