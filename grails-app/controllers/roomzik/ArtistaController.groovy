package roomzik

class ArtistaController {
	def artistaService
    def index() { redirect(action:"consultar")}
    def newArtista(){
    	if (!params.nombre){
    	render(view: "newArtista")
    	}
    	else{	
    		def artista = new Artista(nombre: params.nombre,
    			premium: 1)
    		artistaService.save(artista)
	  		def allsArtista = Artista.getAll()
	  		redirect(action:'consultar')
    	}
    }
    def editMusicas(){
    	if(!params.artistaId){
    		redirect(action :"index")
    	}
    	def artistaId = params.artistaId
    	render(view: "../musica/addMusica",params:[artistaId:artistaId])
    }
    def consultar(){
    	if(params.artistaId){
    		Artista artista = Artista.get(params.artistaId)
    		[artistas: artista, cuantos:1]
    	}
    	else {
    		if(params.artistaIds){

    		}
    		else{
				Set<Artista> alls = Artista.getAll()
				[artistas:alls, cuantos:alls.size()]
			}
		}	 
	}
}
