package roomzik

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
       // "/newArtista"(controller: "Artista", action: "renderNewArtista")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
