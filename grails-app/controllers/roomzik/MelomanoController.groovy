package roomzik

class MelomanoController {
	def melomanoService
    def index() { }
    def newMelomano(){

    	if (!params.nombre){
    	render(view: "newMelomano")
    	}
    	else{
    		if(params.email){
	    		def melomano = new Melomano(nombre: params.nombre,
    				email: params.email)
    			melomanoService.save(melomano)
	  			def allsMelomanos = Melomano.getAll()
	  			render(allsMelomanos)
    		}
    	}
	}
	def consultarAll(){
		render(showMelomano)
		[melomanos: melomanoService.getAll()]
	}
}