package roomzik

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

class MusicaCommand implements Validateable {
    MultipartFile musicaFile
    Long id
    Integer version = 1
    
    static constraints = {
        id nullable: false
        version nullable: false
        musicaFile  validator: { val, obj ->
            if ( val == null ) {
                return false
            }
            if ( val.empty ) {
                return false
            }

            ['mp3'].any { extension -> 
                 val.originalFilename?.toLowerCase()?.endsWith(extension)
            }
        }
    }
}