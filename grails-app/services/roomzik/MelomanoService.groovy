package roomzik

import grails.gorm.transactions.Transactional

@Transactional
class MelomanoService {


       Melomano get(Serializable id){
    	result = Melomano.get(id)
    	return result
    }
    Set<Melomano> getAll(){
    	results = Melomano.getAll()
    	return results
    }

    void delete(Serializable id) {
    	toDelete = Melomano.get(id)
    	if (toDelete) {
    		toDelete.delete()
    	}
    }

    Melomano save(Melomano melomano){
    	melomano.save()
    	return melomano
    }

}