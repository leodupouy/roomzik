package roomzik

import grails.gorm.transactions.Transactional
import roomzik.Artista
@Transactional
class ArtistaService {
    Artista get(Serializable id){
    	def result = Artista.get(id)
    	return result
    }
    Set<Artista> getAll(){
    	def results = Artista.findAll()
    	return results
    }

    void delete(Serializable id) {
    	def toDelete = Artista.get(id)
    	if (toDelete) {
    		toDelete.delete()
    	}
    }
    Artista addMusica(Artista artista,Musica musica){
        artista.creaciones << musica
        artista = save(artista)
        if(artista){
            artista
        }
        else{
            return null
        }
    }
     def save(Artista artista){
    	artista.save()
    }

}
