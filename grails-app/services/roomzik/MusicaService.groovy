package roomzik

import grails.gorm.transactions.Transactional
import java.io.OutputStream
@Transactional
class MusicaService {
    def artistaService

    Musica get(Serializable id){
    	result = Musica.get(id)
    	return result
    }
    Set<Musica> getAll(){
    	results = Musica.getAll()
    	return results
    }

    void delete(Musica musica) {
    	if (musica) {
    		musica.delete()
    	}
    }

    Musica addMusica(MusicaCommand cmd,String applicationPath){
        byte[] bytes = cmd.musicaFile.bytes
        String contentType = cmd.musicaFile.contentType
        String nombre = cmd.musicaFile.originalFilename
        String path = 'audio/'
        File audioDir = new File(applicationPath+'audio');

        // if the directory does not exist, create it
        if (!audioDir.exists()) {
            audioDir.mkdir();
        } 
        
        new FileOutputStream(applicationPath+path+nombre).leftShift( cmd.musicaFile.getInputStream() );
        
        def musica = new Musica(nombre : nombre, path : path,contentType : contentType)
        def artista = Artista.get(cmd.id)
        
        artistaService.addMusica(artista,musica)
        musica.artistas << artista
        musica = save(musica)
        if(musica){
            musica
        }
        else{
            return null
        }
    }

    Musica save(Musica musica){
    	musica.save()
    	return musica
    }
}
