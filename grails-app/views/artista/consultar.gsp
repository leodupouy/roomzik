<!doctype html>
<html>
<head>
	<g:set var="cuantos" value="${cuantos}" />
	<g:set var="artistas" value="${artistas}" />
</head>
<body>
	<g:if test="${cuantos == 0}">
		<div>
			No artistas, puedes anadir uno aca : <g:link action="newArtista"> link </g:link>
			<br>
			hay ${cuantos} artistas
			<br>
	</g:if>
	<g:else>
	<g:if test="${cuantos == 1}">
		<div class="title-body">
			${artistas.nombre}
		</div>
		<br>
		musics :
		${artistas.creaciones}
		<br>
		<div class="list-music">
			<g:each in="${artistas.creaciones}" var="musica">
				<h1>${musica.nombre}</h1>
				<br>
				${musica.musicaContentType}
				<br>
				<g:if test="${musica}">
                <audio controls>
  					<source src="<g:resource dir='audio' file='${musica.nombre}'/>" type="audio/mpeg">
  					
					Your browser does not support the audio element.
				</audio> 

				<br>
				<g:link controller="Musica" action="delete" params="[musicaId:musica.id]" >
				delete music
				</g:link>
				<br> 
            </g:if>
            <br>
			</g:each>
		</div>
		<br>
		<div class="add-music">
			<g:link controller="Artista" action="editMusicas" params="[artistaId: artistas.id]">
				Anadir una musica
			</g:link>
			<br>
		</div>
		puedes anadir un artista aca : <g:link action="newArtista"> link </g:link>
	</g:if>
    <g:else>
    	<div>
       		<bold>Consultar artistas</bold> <br>
    	</div>
    	<g:each in="${artistas}" var="artista">
    		<g:link controller="Artista" action="consultar" params="[artistaId:artista.id]"> 
    			${artista.nombre}
    		</g:link>
    		<br>
	    </g:each>
    </g:else>
    puedes anadir un artista aca : <g:link action="newArtista"> link </g:link>
		</g:else>
</body>
</html>
