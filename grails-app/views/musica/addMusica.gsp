<!doctype html>
<html>
<head>
	<g:set var="artistaId" value="${params.artistaId}" />
</head>
<body>
	<g:uploadForm name="addMusica" controller="Musica" action="addMusica">
	    <g:hiddenField name="id" value="${artistaId}" />
	    <g:hiddenField name="version" value="1" />
	    <input type="file" name="musicaFile" />
	    <fieldset class="buttons">
	        <input class="save" type="submit" value="${message(code: 'musica.upload.label', default: 'Upload')}" />
	    </fieldset>
	</g:uploadForm>
	
</body>
</html>
